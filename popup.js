//------------------CODE RELATED TO THE OPTIONS MENU------------------//

//VARIABLE USED TO STORE THE URL TO BE SCRAPED
var wikiUrlTitle = '';

//PICK UP THE SAVED DATA (URL TO BE CRAWLED)
function restore_options() {
    chrome.storage.sync.get({

        urlToScrape: '',
    }, function(items) {
    	//COLLECT SAVED DATA
        window.wikiUrlTitle = items.urlToScrape;
        wikiUrlTitle = window.wikiUrlTitle;
    });
}
//THIS RUNS WHEN OPENING THE POPUP
document.addEventListener('DOMContentLoaded', restore_options());

//------------------CREATING ONCLICKS FOR STATIC ELEMENTS------------------//

//THIS RUNS WHEN OPENING THE POPUP
document.addEventListener("DOMContentLoaded", function() {
    chrome.tabs.getSelected(null, (tab) => {
        //PICK UP THE CURRENT TAB URL
        let url = tab.url;
        let key = url.split("/browse/")[1];
        //FILTER OUT FILTER PARAMETER
        try {
            key = key.split("?filter")[0];
        } catch {

        }
        //CHECK THAT WE HAVE AN AFM KEY AND THAT THE wikiUrlTitle IS NOT LEFT BLANK
        if (key && wikiUrlTitle != '') {
            this.getElementById('title').innerText = "Adding label/comment to\n" + key;
            this.getElementById('menu').hidden = false;
            App(key);
        } 
        //DISPLAY MESSAGE TO PROVIDE wikiUrlTitle IF NONE ALREADY SAVED
        else if (wikiUrlTitle === '') {
        	this.getElementById('title').innerText = "No wiki page provided for labels/comments.\n\nPlease add the title of the wiki page you want to get data from in the Options page.";
        	this.getElementById('menu').hidden = true;
        }
        //DISPLAY MESSAGE THAT USER NEEDS TO BE ON AN TICKET PAGE
        else {
            this.getElementById('menu').hidden = true;
            this.getElementById('title').innerText = "No ticket was found !\n\nMake sure that your current tab is the Mechabugs tab you want to apply labels/comments to.";
        }
    });
    //LINK submit() FUNCTION TO THE SUBMIT BUTTON
    this.getElementById('submit').onclick = () => {
        submit();
    }
    //OPTIONS BUTTON SUPPORT IN POPUP
    this.getElementById('go-to-options').onclick = () => {
        console.log("options options")
        window.open(chrome.runtime.getURL('options.html'));
    }

});

//------------------SCRAPING THE DATA FROM THE WIKI PAGE AND DISPLAYING IT--------------------//

//FUNCTION TO CREATE THE APP DATA, SCRAPING, PARSING, CREATING ELEMENTS
function App(key) {
    let app = document.getElementById('app');
    app.innerText = key;
    //GET JSON PARSED DATA USING THE getFeedbackOptions() FUNCTION
    getFeedbackOptions()
        .then((data) => {
        	//PARSE THE GIVEN DATA USING THE parserPage() FUNCTION
            let options = parserPage(data);
            //CREATING DISPLAY ELEMENTS USING THE displayOptions() FUNCTION
            displayOptions(app, options);
        })
        .catch((err) => {
            app.innerText = err;
        });
}

//GET THE DATA FROM THE WIKI PAGE USING THE wikiUrlTitle VARIABLE DEFINED IN OPTIONS
function getFeedbackOptions() {
    return new Promise(function(resolve, reject) {
        let xhr = new XMLHttpRequest();
        //INJECT THE WIKI TITLE PAGE IN THE URL TO SCRAPE
        xhr.open("GET", "https://wiki.indeed.com/rest/api/content?title=" + wikiUrlTitle + "&expand=body.view", true);
        //PARSE DATA
        xhr.onload = function(e) {
            console.log(this.status);
            let data = JSON.parse(this.responseText);
            resolve(data);
        }
        xhr.send();
    });
}

//CREATE ARRAY OF DICTIONNARIES STORING THE DATA
function parserPage(data) {
    let options = [];
    try {
        let dataHTML = data.results[0].body.view.value;
        let dataDoc = new DOMParser().parseFromString(dataHTML, "text/html");
        let table = dataDoc.getElementsByTagName('tbody')[0];
        let rows = table.children;

        //LOOP THROUGH ALL DATA GIVEN FROM THE getfeedbackOptions()
        for (var i = 0; i < rows.length; i++) {
            let row = rows[i].children;
            let comment = row[2].innerText;
            if (row[2].innerHTML.match("<a href")) {
                comment = "";
                let children = row[2].childNodes;
                for (var y = 0; y < children.length; y++) {
                    if (children[y].tagName === "A") {
                        comment += "[" + children[y].innerText + "|" + children[y].href + "]";
                    } 
                    else {
                        comment += children[y].textContent;
                    }
                }
            }
            //PUSH ALL DATA IN DICTIONNARIES (ALL DICTIONNARIES ARE IN AN ARRAY)
            options.push({
                name: row[0].innerText,
                tag: row[1].innerText,
                comment: comment
            })
        }
    }

    //IF THE FUNCTION CANNOT PARSE THE GIVEN DATA IT WILL RETURN ONE SINGLE DICTIONNARY IN THE ARRAY, THE ONE BELLOW WITH; name: "NOPE"
    catch (err) {
        options.push({
            name: "NOPE",
            tag: "",
            comment: ""
        })
    }
    return options;
}

//LOOPING THROUGH THE DATA GIVEN BY THE parserPage() FUNCTION WITH THE option() FUNCTION
function displayOptions(app, data) {
    app.innerHTML = "";
    for (var i = 0; i < data.length; i++) {
        let htmlTag = option(data[i]);
        app.append(htmlTag);
    }
}

//CREATING HTML TAGS TO DISPLAY DATA IN THE EXTENSION POPUP
function option(data) {
    //CREATE DIV THAT WILL INCLUDE TITLE, SUBTITLE OR COMMENT/LABEL DATA 
    let div = document.createElement("div");

    //IF PARSER FUNCTION RETURNS NO PARSED DATA
    if (data.name === "NOPE") {
    	document.getElementById("title").innerText = "You are not logged in the wiki.\n\nPlease make sure you are logged in to the wiki and try again (you can do so by clicking the AggOps banner here above).";
        document.getElementById("format").innerText = "\n\INFO: If you are logged in correctly and are still seing this message please check the format of the wiki page you have provided. You can reach the formatting guidelines by clicking the AggOps banner here above.";
        document.getElementById('menu').hidden = true;
    }
    //IF PARSER FOUND DATA
    else {
        //REGEX PATTERNS FOR TITLES/SUBTITLES
        var patt = new RegExp("^TITLE:")
        var subpatt = new RegExp("^SUBTITLE:")

        //IF TITLE
        if (patt.test(data.name)) {
            div.className = "title";
            div.append(data.name.split(":").pop());
        }
        //IF SUBTITLE
        else if (subpatt.test(data.name)) {
            div.className = "subtitle";
            div.append(data.name.split(":").pop());
        } 
        //IF COMMENT/LABEL DATA
        else {
            div.className = "option";
            let check = document.createElement("input");
            check.type = "checkbox";
            check.className = "check";

            div.append(check);
            div.innerHTML += data.name;

            div.onclick = (e) => {
                if (e.target.className === "check") {
                    generateComment();
                }
            }

            if (data.comment === "[Personalize based on specific interaction]") {
                div.innerHTML += " (Include comment below)";
                data.comment = null;
            }
            else {
                let hover = new Hover("Tag: " + data.tag + "\n\n" + data.comment);
                div.append(hover);
            }

            div.setAttribute("data", JSON.stringify(data));
        }
    }
    return div;
}

//ADDING THE HOVER FUNCTIONNALITY TO THE TAGS
function Hover(text) {
    let hoverIcon = document.createElement('div');
    hoverIcon.innerText = "?";
    hoverIcon.className = "hoverIcon";

    let hoverBox = document.createElement('div');
    hoverBox.innerText = text;
    hoverBox.hidden = true;
    hoverBox.className = "hoverBox";

    hoverIcon.onmouseenter = function(e) {
        hoverBox.style.top = (e.clientY - 10) + "px";
        hoverBox.hidden = false;
    }

    hoverIcon.onmouseleave = function() {
        hoverBox.hidden = true;
    }

    hoverIcon.append(hoverBox);
    return hoverIcon;
}

//ADD COMMENT TO THE COMMENT SECTION OF THE POPUP WHEN ONE IS SELECTED
function generateComment() {
    let checks = document.getElementsByClassName('check');
    let comments = [];

    for (var i = 0; i < checks.length; i++) {
        if (checks[i].checked) {
            let data = JSON.parse(checks[i].parentNode.getAttribute("data"));
            data.comment && comments.push(data.comment);
            console.log(checks[i].parentNode.getAttribute("data"));
        }
    }

    let commentDiv = document.getElementById("commentDiv");
    commentDiv.value = comments.join('\n\n');
}

//ADD TYPED COMMENT DATA TO THE COMMENT FROM THE WIKI PAGE
function submit() {
    let checks = document.getElementsByClassName('check');
    let tags = [];

    for (var i = 0; i < checks.length; i++) {
        if (checks[i].checked) {
            let data = JSON.parse(checks[i].parentNode.getAttribute("data"));
            data.tag && tags.push(data.tag);
        }
    }
    //CAPTURES THE DATA FROM THE POPUP HTML ELEMENT SO ALL TYPED DATA IS ALSO ADDED TO THE TICKET
    let commentDiv = document.getElementById("commentDiv");
    //CALL THE addToTicket() FUNCTION
    tags.length >= 0 && addToTicket(tags, commentDiv.value);
}

//CALLS THE FUNCTION FROM THE INSERT.JS FILE USED TO PUCH DATA TO MECHABUGS
function addToTicket(tags, comments) {
    chrome.tabs.query({
        active: true,
        currentWindow: true
    }, function(tab) {
        chrome.tabs.executeScript(tab[0].id, {
            file: 'insert.js'
        }, () => {
            chrome.tabs.sendMessage(tab[0].id, {
                data: {
                    tags: tags,
                    comments: comments
                }
            });
        });
    });
}



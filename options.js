// Saves options to chrome.storage
function save_options() {
  var url = document.getElementById('urlInput').value;
  chrome.storage.sync.set({
    urlToScrape: url,
  }, function() {
    // Update status to let user know options were saved.
    var status = document.getElementById('status');
    status.textContent = 'Options saved.';
    setTimeout(function() {
      status.textContent = '';
    }, 750);
  });
}


// Restores select box and checkbox state using the preferences
// stored in chrome.storage.
function restore_options() {
  // Use default value urlToScrape = ''
  chrome.storage.sync.get({
    urlToScrape: '',
  }, function(items) {
    document.getElementById('urlInput').value = items.urlToScrape;

  });
}

//Runs the restore_option to load any saved data
document.addEventListener('DOMContentLoaded', restore_options);

//Create save_option on the save button
document.getElementById('save').addEventListener('click',
    save_options);

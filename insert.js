(function() {
	chrome.runtime.onMessage.addListener(function(message, sender, sendResponse) {
		handleMessage(message.data);
		chrome.runtime.onMessage.removeListener(arguments.callee);
	});
})();

function handleMessage(data){
    let key = location.href.split("/browse/")[1];
    key = key.split("?filter")[0];
    let url = 'https://mechabugs.indeed.com/rest/api/2/issue/'+key;
    let loaded = 0;
	let count = 0;

	if(data.tags){
		count ++;
		fetch(url, {
	        method: "GET"
	    })
	        .then(res => res.json())
	        .then((ticket) => {
	            var oldLabels = ticket.fields.labels || [];
	            oldLabels = oldLabels.concat(data.tags);

	            var labels = {
	                fields: {
	                    labels: oldLabels
	                }
	            }

	            fetch(url, {
	                method: "PUT",
	                headers: {
	                    "Content-Type": "application/json"
	                },
	                body: JSON.stringify(labels)
	            }).then(() => {
	                loaded ++;
	                loadCheck(count, loaded)
	            });
	        });
	}


	if(data.comments){
		count ++;
		let comment = data.comments;

		try{
			comment = "Hello,\n\n" + comment;
		}catch{
			console.log("No user name found.");
		}

	    fetch(url+"/comment", {
	        method: "POST",
	        body: JSON.stringify({body: comment}),
	        headers: {
	            "Content-Type": "application/json"
	        }
	    }).then(() => {
	        loaded ++;
	        loadCheck(count, loaded)
	    });
	}
}

function loadCheck(count, loaded){
    if(loaded >= count){
        location.reload();
    }
}
